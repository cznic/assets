# assets

Command assets compiles a directory content to a Go file.

## Build status

available at https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fassets

Installation

    $ go get [-u] modernc.org/assets

Documentation: [godoc.org/modernc.org/assets](http://godoc.org/modernc.org/assets)
